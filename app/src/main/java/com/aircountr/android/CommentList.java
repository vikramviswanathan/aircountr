package com.aircountr.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aircountr.android.adapters.CommentListAdapter;
import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.constants.UrlConstants;
import com.aircountr.android.objects.InvoiceListItem;
import com.aircountr.android.prefrences.AppPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Saahil on 16/06/16.
 */
public class CommentList extends BaseActivity implements UrlConstants {
    private TextView tv_msgText;
    private String TAG = this.getClass().getSimpleName();
    private ListView lv_commentList;
    private ArrayList<InvoiceListItem> commentDataList;
    private InvoiceListItem rowData;
    private CommentListAdapter mCommentListAdapter;
    private String strTimeStamp = "", strDay = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commentlist);
        init();
        Intent intent = getIntent();
        strTimeStamp = intent.getStringExtra(TIMESTAMP);
        strDay = intent.getStringExtra(DAY_OF_MONTH);


        commentDataList = new ArrayList<>();
        mCommentListAdapter = new CommentListAdapter(CommentList.this, commentDataList);
        lv_commentList.setAdapter(mCommentListAdapter);
        mCommentListAdapter.onDataSetChanged(commentDataList);

        if (strTimeStamp != null && !strTimeStamp.equals("") && strDay != null && !strDay.equals(""))
            sendInvoiceListRequest(strTimeStamp, strDay);
        else showDialog("Alert", "oh! something went wrong", "OK");
    }
    private void init() {
        lv_commentList = (ListView) findViewById(R.id.lv_invoiceListcom);
        tv_msgText = (TextView) findViewById(R.id.tv_msgTextcom);

        tv_msgText.setTypeface(SEMIBOLD);
    }

    private void sendInvoiceListRequest(String timeStamp, String day) {
        if (!AppPreferences.getMerchantId(CommentList.this).equals("") && !AppPreferences.getAccessToken(CommentList.this).equals("")) {
            if (isNetworkAvailable()) {
                new CommentListAsync().execute(timeStamp, day);
            } else
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        } else {
            showDialog("Alert", "You seems to be logged out", "OK");
        }
    }

    private class CommentListAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz Wait...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.d(TAG, "Response : " + s);
            commentDataList.clear();
            if (s != null) {
                try {
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    if (success) {
                        JSONArray invoiceArray = response.getJSONArray("invoice");
                        if (invoiceArray != null && invoiceArray.length() > 0) {
                            for (int i = 0; i < invoiceArray.length(); i++) {
                                JSONObject invoiceObj = invoiceArray.getJSONObject(i);
                                rowData = new InvoiceListItem();
                                String _comments = invoiceObj.getString("comments");
                                String _vendorName = invoiceObj.getString("vendorname");

                                rowData.setComment(_comments);
                                rowData.setVendorName(_vendorName);

                                commentDataList.add(rowData);
                            }
                            tv_msgText.setVisibility(View.GONE);
                            mCommentListAdapter.onDataSetChanged(commentDataList);
                        } else {
                            tv_msgText.setVisibility(View.VISIBLE);
                            tv_msgText.setText("No comments found");
                        }
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(CommentList.this);
                        builder.setMessage("No Comments Found for the given Date")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        CommentList.this.finish();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                } catch (JSONException e) {
                    AircountrApplication.getInstance().trackException(e);
                    e.printStackTrace();
                }
            } else {
                tv_msgText.setVisibility(View.VISIBLE);
                tv_msgText.setText("Sorry! found nothing");
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.invoiceListUrl(AppPreferences.getMerchantId(CommentList.this), params[0], params[1]);
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", AppPreferences.getAccessToken(CommentList.this));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                AircountrApplication.getInstance().trackException(e);
                e.printStackTrace();
            }
            return _response;
        }
    }
}

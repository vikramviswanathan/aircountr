package com.aircountr.android.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aircountr.android.AddVendorActivity;
import com.aircountr.android.AircountrApplication;
import com.aircountr.android.MyVendorsActivity;
import com.aircountr.android.R;
import com.aircountr.android.adapters.VendorsListAdapter;
import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.constants.UrlConstants;
import com.aircountr.android.objects.CategoryListItem;
import com.aircountr.android.prefrences.AppPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by gaurav on 4/29/2016.
 */
public class VendorsListFragment extends Fragment implements UrlConstants, VendorsListAdapter.VendorsEventClickListener {
    private ListView lv_vendors;
    private TextView tv_msg;
    private ImageView iv_addVendorBtn;
    private TextView tv_addVendorText;
    public ArrayList<CategoryListItem.Vendors> mVendorsList;
    public String categoryId = "", categoryName = "";
    private VendorsListAdapter mVendorsListAdapter;
    private static final int PERMISSIONS_REQUEST_CALL_PHONE = 201;
    private String phoneNumber = "";
    private String TAG = this.getClass().getSimpleName();
    private CategoryListItem rowData;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tv_msg.setTypeface(((MyVendorsActivity) getActivity()).SEMIBOLD);
        tv_addVendorText.setTypeface(((MyVendorsActivity) getActivity()).REGULAR);

        if (null != mVendorsList && mVendorsList.size() > 0) {
            tv_msg.setVisibility(View.GONE);
            mVendorsListAdapter = new VendorsListAdapter(getActivity(), mVendorsList);
            lv_vendors.setAdapter(mVendorsListAdapter);
            mVendorsListAdapter.setVendorClickEventListener(this);
            mVendorsListAdapter.onDataSetChanged(mVendorsList);
        } else {
            tv_msg.setVisibility(View.VISIBLE);
            tv_msg.setText("No vendor found.");
        }

        iv_addVendorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!categoryId.equals("") && !categoryName.equals("")) {
                    Intent intent = new Intent(getActivity(), AddVendorActivity.class);
                    intent.putExtra(CATEGORY_ID, categoryId);
                    intent.putExtra(CATEGORY_NAME, categoryName);
                    intent.putExtra("add", 1);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    Toast.makeText(getActivity(), "Category missing", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_vendors_list, null);
        lv_vendors = (ListView) root.findViewById(R.id.lv_vendors);
        tv_msg = (TextView) root.findViewById(R.id.tv_msg);
        iv_addVendorBtn = (ImageView) root.findViewById(R.id.iv_addVendorBtn);
        tv_addVendorText = (TextView) root.findViewById(R.id.tv_addVendorText);
        final Bitmap src = BitmapFactory.decodeResource(getResources(), R.drawable.icon_add_category2);
        final Bitmap shadows = imgshadow(src, 100, 100, getResources().getColor(R.color.shadow), 1, 6, 10);
        iv_addVendorBtn.setImageBitmap(shadows);
        return root;
    }

    @Override
    public void onEventClickListener(int btnType, String phoneNumber, int position) {
        this.phoneNumber = phoneNumber;
        if (btnType == 1) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CALL_PHONE)) {
                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, PERMISSIONS_REQUEST_CALL_PHONE);
                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                startActivity(intent);
            }
        } else if (btnType == 2) {
            Uri uri = Uri.parse("smsto:" + phoneNumber);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(i, ""));
        } else if (btnType == 3) {
            selectOptionPopUp(position);
        }
    }

    private void selectOptionPopUp(final int position) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_delete_update_option_layout);

        TextView title = (TextView) dialog.findViewById(R.id.tv_popUpTitle);
        TextView updateText = (TextView) dialog.findViewById(R.id.tv_updateText);
        TextView deleteText = (TextView) dialog.findViewById(R.id.tv_deleteText);
        LinearLayout deleteBtn = (LinearLayout) dialog.findViewById(R.id.ll_deleteBtn);
        LinearLayout updateBtn = (LinearLayout) dialog.findViewById(R.id.ll_updateBtn);
        TextView deleteIcon = (TextView) dialog.findViewById(R.id.deleteIcon);
        TextView updateIcon = (TextView) dialog.findViewById(R.id.updateIcon);
        title.setTypeface(((MyVendorsActivity) getActivity()).SEMIBOLD);
        deleteText.setTypeface(((MyVendorsActivity) getActivity()).REGULAR);
        updateText.setTypeface(((MyVendorsActivity) getActivity()).REGULAR);
        deleteIcon.setTypeface(((MyVendorsActivity) getActivity()).FONTAWESOME);
        updateIcon.setTypeface(((MyVendorsActivity) getActivity()).FONTAWESOME);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDeleteVendorRequest(mVendorsList.get(position).getVendorId(), String.valueOf(position));
                dialog.dismiss();
            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddVendorActivity.class);
                intent.putExtra(CATEGORY_ID, ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.get(position).getCategoryId());
                intent.putExtra(CATEGORY_NAME, ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.get(position).getCategoryName());
                intent.putExtra("add", 0);
                intent.putExtra("vendorname", mVendorsList.get(position).getVendorName());
                intent.putExtra("vendorId", mVendorsList.get(position).getVendorId());
                intent.putExtra("phonenumber", phoneNumber);
                intent.putExtra("vendoraddress", mVendorsList.get(position).getVendorAddress());
                startActivity(intent);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void sendDeleteVendorRequest(String VendorId, String position) {
        if (!AppPreferences.getMerchantId(getActivity()).equals("") && !AppPreferences.getAccessToken(getActivity()).equals("")) {
            if (!categoryId.equals("") && !categoryName.equals("")) {
                if (isNetworkAvailable())
                    new DeleteVendorAsync().execute(VendorId, position);
                else
                    Toast.makeText(getContext(), "No Internet Available", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "oops! something is missing go back and try again", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "oops! you seems logged out", Toast.LENGTH_SHORT).show();
        }
    }

    private class DeleteVendorAsync extends AsyncTask<String, Void, Pair> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ((MyVendorsActivity) getActivity()).showLoading("Adding...", false);
        }

        @Override
        protected void onPostExecute(Pair pair) {
            super.onPostExecute(pair);
            String s = pair.Second;
            Log.d("Vendor Delete", "Response :" + s);
            try {
                JSONObject response = new JSONObject(s);
                boolean success = response.getBoolean("success");
                String msg = response.getString("msg");
                if (success) {
                    mVendorsList.remove(pair.First);
                    mVendorsListAdapter.onDataSetChanged(mVendorsList);
                    Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
                    sendRequestGetCategory();
                } else {
                    ((MyVendorsActivity) getActivity()).showDialog("Alert", msg, "OK");
                }
            } catch (JSONException e) {
                AircountrApplication.getInstance().trackException(e);
                e.printStackTrace();
            }
            ((MyVendorsActivity) getActivity()).hideLoading();
        }

        @Override
        protected Pair doInBackground(String... params) {
            final String url = CreateUrl.deleteVendorUrl();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", AppPreferences.getAccessToken(getActivity()));
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("merchantId", AppPreferences.getMerchantId(getActivity()));
                jsonObject.put("vendorId", params[0]);
                httpPost.setEntity(new ByteArrayEntity(jsonObject.toString().getBytes("UTF8")));

                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                AircountrApplication.getInstance().trackException(e);
                e.printStackTrace();
            } catch (JSONException e) {
                AircountrApplication.getInstance().trackException(e);
                e.printStackTrace();
            }
            return new Pair(Integer.parseInt(params[1]), _response);
        }
    }

    private void sendRequestGetCategory() {
        if (isNetworkAvailable()) {
            if (!AppPreferences.getMerchantId(getActivity()).equals("") && !AppPreferences.getAccessToken(getActivity()).equals("")) {
                new GetCategoriesAsync().execute(AppPreferences.getMerchantId(getActivity()), AppPreferences.getAccessToken(getActivity()));
            } else {
                Toast.makeText(getContext(), "oops! something is missing go back and try again", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "oops! you seems logged out", Toast.LENGTH_SHORT).show();
        }
    }

    private class GetCategoriesAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ((MyVendorsActivity) getActivity()).showLoading("Loading...", false);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.clear();
            } catch (Exception e) {

            }
            if (result != null) {

                try {
                    Log.d(TAG, "Response : " + result);
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    if (success) {
                        JSONArray categoryArray = response.getJSONArray("category");
                        if (categoryArray != null && categoryArray.length() > 0) {
                            for (int i = 0; i < categoryArray.length(); i++) {
                                JSONObject categoryData = categoryArray.getJSONObject(i);
                                rowData = new CategoryListItem();
                                String categoryId = categoryData.getString("_id");
                                String resourceId = categoryData.getString("url");
                                String merchantId = categoryData.getString("merchantId");
                                String categoryName = categoryData.getString("name");
                                boolean isVisible = categoryData.getBoolean("view");
                                JSONArray vendorsArray = categoryData.getJSONArray("vendors");
                                ArrayList<CategoryListItem.Vendors> vendorsList = new ArrayList<>();
                                if (vendorsArray != null && vendorsArray.length() > 0) {
                                    for (int j = 0; j < vendorsArray.length(); j++) {
                                        JSONObject vendor = vendorsArray.getJSONObject(j);
                                        CategoryListItem.Vendors vRowData = rowData.new Vendors();
                                        boolean vVendorView = false;
                                        if (vendor.has("view"))
                                            if (vendor.getBoolean("view"))
                                                vVendorView = vendor.getBoolean("view");

                                        String vCategoryId = vendor.getString("categoryId");
                                        String vCategory = vendor.getString("category");
                                        String vMerchantId = vendor.getString("merchantId");
                                        String vAddress = vendor.getString("address");
                                        String vNumber = vendor.getString("number");
                                        String vName = vendor.getString("name");
                                        String vId = vendor.getString("_id");

                                        if (vVendorView) {
                                            vRowData.setCategoryId(vCategoryId);
                                            vRowData.setCategory(vCategory);
                                            vRowData.setMerchantId(vMerchantId);
                                            vRowData.setVendorAddress(vAddress);
                                            vRowData.setVendorNumber(vNumber);
                                            vRowData.setVendorName(vName);
                                            vRowData.setVendorId(vId);
                                            vendorsList.add(vRowData);
                                        }
                                    }
                                }
                                rowData.setCategoryId(categoryId);
                                rowData.setResourceId(resourceId);
                                rowData.setMerchantId(merchantId);
                                rowData.setCategoryName(categoryName);
                                rowData.setIsVisible(isVisible);
                                rowData.setVendorsDataList(vendorsList);
                                try {
                                    ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.add(rowData);
                                } catch (Exception e) {
                                }
                            }
                        }
                    } else {
                        String message = response.getString("msg");
                        ((MyVendorsActivity) getActivity()).showDialog("Alert", message, "OK");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            ((MyVendorsActivity) getActivity()).hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.getCategoriesUrl(params[0]);
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", params[1]);
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                    startActivity(intent);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    public Bitmap imgshadow(final Bitmap bm, final int dstHeight, final int dstWidth, int color, int size, float dx, float dy) {
        final Bitmap mask = Bitmap.createBitmap(dstWidth, dstHeight, Bitmap.Config.ALPHA_8);

        final Matrix scaleToFit = new Matrix();
        final RectF src = new RectF(0, 0, bm.getWidth(), bm.getHeight());
        final RectF dst = new RectF(0, 0, dstWidth - dx, dstHeight - dy);
        scaleToFit.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER);

        final Matrix dropShadow = new Matrix(scaleToFit);
        dropShadow.postTranslate(dx, dy);

        final Canvas maskCanvas = new Canvas(mask);
        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        maskCanvas.drawBitmap(bm, scaleToFit, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
        maskCanvas.drawBitmap(bm, dropShadow, paint);

        final BlurMaskFilter filter = new BlurMaskFilter(size, BlurMaskFilter.Blur.NORMAL);
        paint.reset();
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setMaskFilter(filter);
        paint.setFilterBitmap(true);

        final Bitmap ret = Bitmap.createBitmap(dstWidth, dstHeight, Bitmap.Config.ARGB_8888);
        final Canvas retCanvas = new Canvas(ret);
        retCanvas.drawBitmap(mask, 0, 0, paint);
        retCanvas.drawBitmap(bm, scaleToFit, null);
        mask.recycle();
        return ret;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    private class Pair {
        int First;
        String Second;

        public Pair(int First, String Second) {
            this.First = First;
            this.Second = Second;
        }
    }


}

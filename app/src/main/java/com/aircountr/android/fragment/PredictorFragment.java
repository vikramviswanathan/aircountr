package com.aircountr.android.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aircountr.android.ExpenseActivity;
import com.aircountr.android.R;
import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.objects.PredictorChartDataItem;
import com.aircountr.android.prefrences.AppPreferences;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class PredictorFragment extends Fragment implements ValueFormatter{

    private String TAG = this.getClass().getSimpleName();
    private Calendar cal;
    private String strTimeStamp = null;
    private TextView tv_predictedExpense,tv_text_actual,tv_actualExpense,tv_text_predicted;
    private LineChart lineChart;
    private PredictorChartDataItem[] predictorChartDataItems=new PredictorChartDataItem[32];
    private int maxDays,today,totalActualExpenses,daysCompleted=0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_predictor, null);
        tv_actualExpense=(TextView)root.findViewById(R.id.tv_actualexpense);
        tv_predictedExpense=(TextView)root.findViewById(R.id.tv_predictedexpense);
        tv_text_actual=(TextView)root.findViewById(R.id.tv_text_actual);
        tv_text_predicted=(TextView)root.findViewById(R.id.tv_text_predicted);
        lineChart = (LineChart)root.findViewById(R.id.lc_expense);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tv_actualExpense.setTypeface(((ExpenseActivity) getActivity()).SEMIBOLD);
        tv_predictedExpense.setTypeface(((ExpenseActivity) getActivity()).SEMIBOLD);
        tv_text_actual.setTypeface(((ExpenseActivity) getActivity()).REGULAR);
        tv_text_predicted.setTypeface(((ExpenseActivity) getActivity()).REGULAR);
        cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        today=cal.get(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        maxDays=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for(int i=0;i<32;i++)
        {
            predictorChartDataItems[i]=new PredictorChartDataItem();
        }
        predictorChartDataItems[0].setActualamount(0);
        strTimeStamp = String.valueOf(cal.getTimeInMillis());
        totalActualExpenses=0;
        sendGetCalendarRequest(strTimeStamp);
    }

    private void setData(){
        ArrayList<Entry> actual_expenses_entries = new ArrayList<>();
        ArrayList<Entry> predicted_expenses_entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();

        for(int i=1;i<=maxDays;i++)
        {
            actual_expenses_entries.add(new Entry(predictorChartDataItems[i].getActualamount(),i-1));
            predicted_expenses_entries.add(new Entry(predictorChartDataItems[i].getPredictedamount(),i-1));
            labels.add(Integer.toString(i));
        }
        LineDataSet actualExpensesDataSet = new LineDataSet(actual_expenses_entries, "Actual Expenses");
        actualExpensesDataSet.setColor(ColorTemplate.getHoloBlue());
        actualExpensesDataSet.setLineWidth(2f);
        actualExpensesDataSet.setDrawCircleHole(false);
        actualExpensesDataSet.setDrawCircles(false);
        actualExpensesDataSet.setValueFormatter(this);

        LineDataSet predictedExpensesDataSet = new LineDataSet(predicted_expenses_entries, "Predicted Expenses");
        predictedExpensesDataSet.setColor(Color.GREEN);
        predictedExpensesDataSet.setLineWidth(2f);
        predictedExpensesDataSet.setDrawCircleHole(false);
        predictedExpensesDataSet.setDrawCircles(false);
        predictedExpensesDataSet.setValueFormatter(this);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(actualExpensesDataSet); // add the datasets
        dataSets.add(predictedExpensesDataSet);

        LineData data = new LineData(labels, dataSets);

        lineChart.setData(data);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.setDescription("Actual Vs Predicted Expense");
        lineChart.notifyDataSetChanged();
        lineChart.requestFocus();
        lineChart.setActivated(true);
        lineChart.performClick();
    }

    @Override
    public void onResume() {
        super.onResume();
        lineChart.performClick();
    }

    private void sendPredictorChartRequest(Pair pair) {
        if (!AppPreferences.getMerchantId(getActivity()).equals("") && !AppPreferences.getAccessToken(getActivity()).equals("")) {
            if ((((ExpenseActivity) getActivity()).isNetworkAvailable())) {
                new PredictorChartAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, String.valueOf(pair.First),pair.Second);
            } else {
                ((ExpenseActivity) getActivity()).showDialog("Error", getActivity().getResources().getString(R.string.no_internet), "OK");
            }
        } else {
            ((ExpenseActivity) getActivity()).showDialog("Alert", "Oh! you seems logged out", "OK");
        }
    }

    @Override
    public String getFormattedValue(float v, Entry entry, int i, ViewPortHandler viewPortHandler) {
        return "";
    }

    private class PredictorChartAsync extends AsyncTask<String, Void, Pair> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Pair resp) {
            super.onPostExecute(resp);
            int first=resp.First;
            String s=resp.Second;
            Log.d(TAG, "Response :" + s);
            if (s != null) {
                try {
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    if(success)
                    {
                        JSONArray dataArray = response.getJSONArray("data");
                        if (dataArray != null && dataArray.length() > 0) {
                            JSONObject jsonObject=dataArray.getJSONObject(0);
                            int predictedExpense=jsonObject.getInt("predictedexpense");
                            if(first==today) {
                                tv_text_actual.setText("Your actual expenses for month of "+ cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) + " till date is");
                                tv_actualExpense.setText(Integer.toString(totalActualExpenses));
                                tv_text_predicted.setText("Your predicted expenses for month of " + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH) + " could be");
                                tv_predictedExpense.setText(Integer.toString(predictedExpense));
                            }
                            predictorChartDataItems[first].setPredictedamount(predictedExpense);
                        }
                    }
                    daysCompleted++;
                    if(daysCompleted==maxDays)
                    {
                        setData();
                        ((ExpenseActivity) getActivity()).hideLoading();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ((ExpenseActivity) getActivity()).hideLoading();
                }
            }
            else
            {
                ((ExpenseActivity) getActivity()).hideLoading();
            }
        }

        @Override
        protected Pair doInBackground(String... params) {

            final String url;
            url= CreateUrl.predictorChartUrl(AppPreferences.getMerchantId(getActivity()), params[1]);
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", AppPreferences.getAccessToken(getActivity()));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new Pair(Integer.parseInt(params[0]),_response);
        }
    }
    private void sendGetCalendarRequest(String timeStamp) {
        if (!AppPreferences.getMerchantId(getActivity()).equals("") && !AppPreferences.getAccessToken(getActivity()).equals("")) {
            if(((ExpenseActivity) getActivity()).isNetworkAvailable()) {
                new GetCalendarAsync().execute(timeStamp);
            } else
                ((ExpenseActivity) getActivity()).showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        } else {
            ((ExpenseActivity) getActivity()).showDialog("Alert", "You seems to be logged out", "OK");
        }
    }

    private class GetCalendarAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ((ExpenseActivity) getActivity()).showLoading("Plz Wait...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG, "Response : " + s);
            try {
                if (s != null) {
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    if (success) {
                        JSONArray invoiceArray = response.getJSONArray("inv");
                        if (invoiceArray != null && invoiceArray.length() > 0) {
                            for (int i = 0; i < invoiceArray.length(); i++) {
                                JSONObject invoiceObj = invoiceArray.getJSONObject(i);
                                JSONObject dayObj = invoiceObj.getJSONObject("_id");
                                int day = dayObj.getInt("day");
                                int amount = invoiceObj.getInt("totalinvoice");
                                if (day > 0)
                                    predictorChartDataItems[day].setActualamount(amount);
                                if(day<=today)
                                    totalActualExpenses+=amount;
                            }
                        }
                    }
                    for(int i=1;i<=maxDays;i++) {
                        sendPredictorChartRequest(new Pair(i,String.valueOf(cal.getTimeInMillis())));
                        cal.add(Calendar.DAY_OF_MONTH,1);
                    }
                } else {
                    ((ExpenseActivity) getActivity()).hideLoading();
                    ((ExpenseActivity) getActivity()).showDialog("Alert", "oops! something is wrong", "OK");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ((ExpenseActivity) getActivity()).hideLoading();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.getCalendarDataUrl(AppPreferences.getMerchantId(getActivity()), params[0], "", "");
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", AppPreferences.getAccessToken(getActivity()));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }
    private class Pair{
        int First;
        String Second;
        public Pair(int First,String Second){
            this.First=First;
            this.Second=Second;
        }
    }
}
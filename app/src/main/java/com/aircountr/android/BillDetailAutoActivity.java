package com.aircountr.android;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.constants.UrlConstants;
import com.aircountr.android.prefrences.AppPreferences;
import com.aircountr.android.utils.AircountrUtils;
import com.splunk.mint.Mint;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by gaura on 5/23/2016.
 */
public class BillDetailAutoActivity extends BaseActivity implements UrlConstants, UploadStatusDelegate {
    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private ImageView iv_imgBtn;
    private EditText et_comment;
    private TextView tv_saveBtn;
    private final int CAMERA_REQUEST = 101, GALLERY_REQUEST = 100;
    private byte[] image_byte;
    private ByteArrayBody bab;
    private String categoryId = "", categoryName = "";
    private Uri fileUri, outputUri = null;
    private static final String IMAGE_DIRECTORY_NAME = "Aircountr";
    private String filePath = "";
    private Map<String, UploadProgressViewHolder> uploadProgressHolders = new HashMap<>();
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int PERMISSION_REQUEST_READ_EXTERNAL_STORAGE = 100;
    private static final int REQUEST_PERMISSION_STORAGE_SETTING = 101;
    private static final int REQUEST_TAKE_IMAGE_FROM_GALLERY = 102;
    private static final int REQUEST_TAKE_PICTURE_FROM_CAMERA = 103;
    private ViewGroup vwgrp_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
        Mint.initAndStartSession(BillDetailAutoActivity.this, "058c75a4");
        verifyStoragePermissions(BillDetailAutoActivity.this);
        setContentView(R.layout.activity_bill_detail_auto);

        Intent intent = getIntent();
        categoryId = intent.getStringExtra(CATEGORY_ID);
        categoryName = intent.getStringExtra(CATEGORY_NAME);

        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        iv_imgBtn = (ImageView) findViewById(R.id.iv_imgBtn);
        et_comment = (EditText) findViewById(R.id.et_comment);
        tv_saveBtn = (TextView) findViewById(R.id.tv_saveBtn);
        vwgrp_container = (ViewGroup) findViewById(R.id.container);

        tv_pageTitle.setTypeface(SEMIBOLD);
        et_comment.setTypeface(REGULAR);
        tv_saveBtn.setTypeface(SEMIBOLD);

        iv_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BillDetailAutoActivity.this.finish();
            }
        });

        iv_imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImgOptionPopUp();
            }
        });

        tv_saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (bab != null) {
                    sendBillDetailRequest(categoryId, et_comment.getText().toString());
                } else {
                    showDialog("Alert", "Plz select image", "OK");
                }*/
                final String fileUploadUrl = CreateUrl.uploadBillDetail();
                /*File file = new File(BillDetailAutoActivity.this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "IMG" + System.currentTimeMillis() + ".jpg");
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    Uri outputUri = null;
                    filePath = file.getAbsolutePath();
                    outputUri = Uri.fromFile(file);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
                }
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE_FROM_CAMERA);*/

                try {
                    final String fileName = AircountrUtils.getFileName(filePath);
                    MultipartUploadRequest req = new MultipartUploadRequest(BillDetailAutoActivity.this, fileUploadUrl)
                            .addFileToUpload(filePath, "invoice")
                            .addHeader("Authorization", AppPreferences.getAccessToken(BillDetailAutoActivity.this))
                            .addParameter("vendorId", "")
                            .addParameter("merchantId", AppPreferences.getMerchantId(BillDetailAutoActivity.this))
                            .addParameter("categoryId", categoryId)
                            .addParameter("isautomode", "true")
                            .addParameter("comments", et_comment.getText().toString().trim())
                            .addParameter("categoryname", categoryName)
                            .addParameter("isprocessed", "false")

                        /*if (bab != null)
                            entity.addPart("invoice", bab);*/
                            .setNotificationConfig(getNotificationConfig(fileName))
//                                .setCustomUserAgent(USER_AGENT)
//                                .setAutoDeleteFilesAfterSuccessfulUpload(false)
//                                .setUsesFixedLengthStreamingMode(true)
                            .setMaxRetries(3);

                    String uploadID = req.setDelegate(BillDetailAutoActivity.this).startUpload();
                    switchActivity(BillDetailAutoActivity.this, HomeActivity.class);
                } catch (FileNotFoundException exc) {
                    showToast(exc.getMessage());
                } catch (IllegalArgumentException exc) {
                    showToast("Missing some arguments. " + exc.getMessage());
                } catch (MalformedURLException exc) {
                    showToast(exc.getMessage());
                }
            }
        });
        captureImage();
//        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    private void sendBillDetailRequest(String categoryId, String comment) {
        if (!AppPreferences.getMerchantId(BillDetailAutoActivity.this).equals("") && !AppPreferences.getAccessToken(BillDetailAutoActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new BillDetailAsync().execute(categoryId, comment);
            } else {
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
            }
        } else {
            switchActivity(BillDetailAutoActivity.this, SignInActivity.class);
        }
    }

    private class BillDetailAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz wait...", false);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    String msg = response.getString("msg");
                    if (success) {
                        displayToast(msg);
                        HomeActivity.isInvoiceListChanged = true;
                        BillDetailAutoActivity.this.finish();
                    } else {
                        showDialog("Aler", msg, "OK");
                    }

                } catch (JSONException e) {
                    AircountrApplication.getInstance().trackException(e);
                    e.printStackTrace();
                }
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.uploadBillDetail();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Authorization", AppPreferences.getAccessToken(BillDetailAutoActivity.this));
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            try {
                entity.addPart("merchantId", new StringBody(AppPreferences.getMerchantId(BillDetailAutoActivity.this)));
                entity.addPart("categoryId", new StringBody(params[0]));
                entity.addPart("isautomode", new StringBody("true"));
                entity.addPart("comments", new StringBody(params[1]));
                entity.addPart("categoryname", new StringBody(categoryName));
                entity.addPart("isprocessed", new StringBody("false"));
                entity.addPart("invoice", bab);
                ;

                httpPost.setEntity(entity);
                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                AircountrApplication.getInstance().trackException(e);
                e.printStackTrace();
            }
            Log.d(TAG, "Response : " + _response);
            return _response;
        }
    }

    private void selectImgOptionPopUp() {
        final Dialog dialog = new Dialog(BillDetailAutoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_select_img_layout);
        TextView tv_cameraBtn = (TextView) dialog.findViewById(R.id.tv_cameraBtn);
        TextView tv_galleryBtn = (TextView) dialog.findViewById(R.id.tv_galleryBtn);
        TextView tv_cancelBtn = (TextView) dialog.findViewById(R.id.tv_cancelBtn);

        tv_cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(intent, CAMERA_REQUEST);
                verifyStoragePermissions(BillDetailAutoActivity.this);
                File file = new File(BillDetailAutoActivity.this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "IMG" + System.currentTimeMillis() + ".jpg");
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    Uri outputUri = null;
                    filePath = file.getAbsolutePath();
                    outputUri = Uri.fromFile(file);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
                }
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE_FROM_CAMERA);
                dialog.dismiss();
            }
        });

        tv_galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkIfStorageAccessPermissionsGranted()) {
                    takeImageFromGallery();
                }
//                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//                photoPickerIntent.setType("image/*");
//                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
                dialog.dismiss();
            }
        });

        tv_cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void takeImageFromGallery() {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 19) {
            // For Android versions of KitKat or later, we use a
            // different intent to ensure
            // we can get the file path from the returned intent URI
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        } else {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }

        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_TAKE_IMAGE_FROM_GALLERY);
    }

    /**
     * Check if external storage read permission have been granted.
     *
     * @return {@code true} if permissions grated, {@code false} otherwise
     */
    private boolean checkIfStorageAccessPermissionsGranted() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return true;
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(this)
                    .setTitle("Storage access")
                    .setMessage("Storage access required to take image from the gallery")
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestStoragePermission();
                        }
                    }).create().show();
        } else {
            // No explanation needed, we can request the permission.
            requestStoragePermission();
        }
        return false;
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_READ_EXTERNAL_STORAGE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted,
                    takeImageFromGallery();
                } else {
                    // permission denied!
                    // next time use opens the app show him the empty screen with message to enable the location settings
                    new AlertDialog.Builder(this)
                            .setMessage("Please enable storage access.")
                            .setPositiveButton("Go to settings", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, REQUEST_PERMISSION_STORAGE_SETTING);
                                }
                            })
                            .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).create().show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_TAKE_IMAGE_FROM_GALLERY) {
                Uri uri = data.getData();
                try {
                    String path = getPath(uri);
                    setPictureAndShowInImageView(path);
                } catch (URISyntaxException e) {
                    Toast.makeText(this,
                            "Unable to get the file from the given URI.  See error log for details",
                            Toast.LENGTH_LONG).show();
                    Log.e(TAG, "Unable to upload file from the given uri", e);
                }
            } else if (requestCode == REQUEST_PERMISSION_STORAGE_SETTING) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_READ_EXTERNAL_STORAGE);
            } else if (requestCode == REQUEST_TAKE_PICTURE_FROM_CAMERA) {
                // check if the file to which the picture from camera is saved does exists
                if (!TextUtils.isEmpty(filePath)) {
                    File file = new File(filePath);
                    setPictureAndShowInImageView(filePath);
                    Log.d(TAG, "Picture from camera saved at : " + file.getAbsolutePath());
                }
            }
        }else if(resultCode == Activity.RESULT_CANCELED){
            BillDetailAutoActivity.this.finish();
        }
    }

    private void setPictureAndShowInImageView(String path) {
        filePath = path;
        // set the image into your imageview
        iv_imgBtn.setImageURI(Uri.fromFile(new File(path)));
    }

    /*
     * Gets the file path of the given Uri.
     */
    @SuppressLint("NewApi")
    private String getPath(Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void captureImage() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        /*fileUri = getOutputMediaFileUri(1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, REQUEST_TAKE_PICTURE_FROM_CAMERA);*/
        File file = new File(BillDetailAutoActivity.this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "IMG" + System.currentTimeMillis() + ".jpg");
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            outputUri = null;
            filePath = file.getAbsolutePath();
            outputUri = Uri.fromFile(file);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
        }
        startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE_FROM_CAMERA);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create " + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }

        return mediaFile;
    }


    @Override
    protected void onResume() {
        super.onResume();
        AircountrApplication.getInstance().trackScreenView(TAG);
    }

    private UploadNotificationConfig getNotificationConfig(String filename) {
//        if (!displayNotification.isChecked()) return null;

        return new UploadNotificationConfig()
                .setIcon(R.drawable.ic_upload)
                .setTitle(filename)
                .setInProgressMessage(getString(R.string.uploading))
                .setCompletedMessage(getString(R.string.upload_success))
                .setErrorMessage(getString(R.string.upload_error))
                .setAutoClearOnSuccess(true)
                .setClickIntent(new Intent(this, BillDetailsActivity.class))
                .setClearOnAction(true)
                .setRingToneEnabled(true);
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void addUploadToList(String uploadID, String filename) {
        View uploadProgressView = getLayoutInflater().inflate(R.layout.view_upload_progress, null);
        UploadProgressViewHolder viewHolder = new UploadProgressViewHolder(uploadProgressView, filename);
        viewHolder.uploadId = uploadID;
        vwgrp_container.addView(viewHolder.itemView, 0);
        uploadProgressHolders.put(uploadID, viewHolder);
    }

    class UploadProgressViewHolder {
        View itemView;
        TextView uploadTitle;
        ProgressBar progressBar;
        String uploadId;

        UploadProgressViewHolder(View view, String filename) {
            itemView = view;
            uploadTitle = (TextView) itemView.findViewById(R.id.uploadTitle);
            progressBar = (ProgressBar) itemView.findViewById(R.id.uploadProgress);

            progressBar.setMax(100);
            progressBar.setProgress(0);

            uploadTitle.setText(getString(R.string.upload_progress, filename));
        }
    }

    @Override
    public void onProgress(UploadInfo uploadInfo) {
        Log.i(TAG, String.format(Locale.getDefault(), "ID: %1$s (%2$d%%) at %3$.2f Kbit/s",
                uploadInfo.getUploadId(), uploadInfo.getProgressPercent(),
                uploadInfo.getUploadRate()));
//        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());
//
//        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
//            return;
//
//        uploadProgressHolders.get(uploadInfo.getUploadId())
//                .progressBar.setProgress(uploadInfo.getProgressPercent());
    }

    @Override
    public void onError(UploadInfo uploadInfo, Exception exception) {
        Log.e(TAG, "Error with ID: " + uploadInfo.getUploadId() + ": "
                + exception.getLocalizedMessage(), exception);
//        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());
//
//        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
//            return;
//
//        vwgrp_container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
//        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    @Override
    public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
        Log.i(TAG, String.format(Locale.getDefault(),
                "ID %1$s: completed in %2$ds at %3$.2f Kbit/s. Response code: %4$d, body:[%5$s]",
                uploadInfo.getUploadId(), uploadInfo.getElapsedTime() / 1000,
                uploadInfo.getUploadRate(), serverResponse.getHttpCode(),
                serverResponse.getBodyAsString()));
        //logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());
        for (Map.Entry<String, String> header : serverResponse.getHeaders().entrySet()) {
            Log.i("Header", header.getKey() + ": " + header.getValue());
        }

        if (serverResponse.getHttpCode() == 200) {
            try {
                JSONObject responseObject = new JSONObject(serverResponse.getBodyAsString());
                Log.d("uploaderror",responseObject.toString());
                boolean isSuccess = responseObject.getBoolean("success");
                String message = responseObject.getString("msg");
                Toast.makeText(BillDetailAutoActivity.this, message, Toast.LENGTH_SHORT).show();
                if(isSuccess)
                {
                    HomeActivity.isInvoiceListChanged=true;
                    HomeActivity.checkForUpdate(1);
                }
                BillDetailAutoActivity.this.finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(BillDetailAutoActivity.this, "Error while uploading the invoice.", Toast.LENGTH_SHORT).show();
        }

//        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
//            return;
//
//        vwgrp_container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
//        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    @Override
    public void onCancelled(UploadInfo uploadInfo) {
        Log.i(TAG, "Upload with ID " + uploadInfo.getUploadId() + " is cancelled");
        //logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());

//        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
//            return;
//
//        vwgrp_container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
//        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    private void logSuccessfullyUploadedFiles(List<String> files) {
        for (String file : files) {
            Log.e(TAG, "Success:" + file);
        }
    }

    /**
     * Checks if the app has permission to write to device storage
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}
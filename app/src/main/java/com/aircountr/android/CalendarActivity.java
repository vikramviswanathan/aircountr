package com.aircountr.android;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aircountr.android.adapters.CalendarGridAdapter;
import com.aircountr.android.adapters.CategorySpinnerAdapter;
import com.aircountr.android.adapters.VendorSpinnerAdapter;
import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.constants.UrlConstants;
import com.aircountr.android.objects.CalendarListItem;
import com.aircountr.android.objects.CategoryListItem;
import com.aircountr.android.prefrences.AppPreferences;
import com.splunk.mint.Mint;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by gaurav on 4/27/2016.
 */
public class CalendarActivity extends BaseActivity implements View.OnClickListener, UrlConstants {
    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private ImageView iv_export;
    private TextView tv_categoryText;
    private TextView tv_category;
    private TextView tv_vendorText;
    private TextView tv_vendor;
    private ImageView iv_previousDateBtn;
    private TextView tv_selectedDate;
    private TextView tv_totalexpenses;
    private ImageView iv_nextDateBtn;
    private LinearLayout ll_daysOfWeeks;
    private LinearLayout ll_vendor,ll_category;
    private GridView gv_calendar;
    private TextView tv_errNoDataMsg;
    private int BTN_CATEGORY = 1, BTN_VENDOR = 2;
    private ArrayList<CategoryListItem.Vendors> vendorsDataList;
    private Calendar cal;
    private int total=0,position;
    private CalendarGridAdapter mCalendarGridAdapter;
    private ArrayList<CalendarListItem> mCalendarDataList;
    private String strTimeStamp = null, categoryId = "", vendorId = "";
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
        Mint.initAndStartSession(CalendarActivity.this, "058c75a4");

        setContentView(R.layout.activity_calendar);
        vendorsDataList = new ArrayList<>();
        init();

        cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));

        mCalendarDataList = new ArrayList<>();
        mCalendarGridAdapter = new CalendarGridAdapter(CalendarActivity.this, mCalendarDataList);
        gv_calendar.setAdapter(mCalendarGridAdapter);

        strTimeStamp = String.valueOf(cal.getTimeInMillis());
        sendGetCalendarRequest(String.valueOf(cal.getTimeInMillis()));

        gv_calendar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String x =String.valueOf(mCalendarDataList.get(position).getDate());
                if (strTimeStamp != null && !strTimeStamp.equals("") && x != null && !x.equals("")) {
                    sendInvoiceListRequest(strTimeStamp, x);
                    setPosition(position);
                }
            }
        });
        gv_calendar.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(CalendarActivity.this, CommentList.class);
                intent.putExtra(TIMESTAMP, strTimeStamp);
                intent.putExtra(DAY_OF_MONTH, String.valueOf(mCalendarDataList.get(position).getDate()));
                startActivity(intent);
                return true;
            }
        });
    }

    private void init() {
        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        iv_export = (ImageView) findViewById(R.id.iv_export);
        tv_category = (TextView)findViewById(R.id.tv_category);
        tv_categoryText = (TextView) findViewById(R.id.tv_categoryText);
        tv_vendor = (TextView)findViewById(R.id.tv_vendor);
        tv_vendorText = (TextView) findViewById(R.id.tv_vendorText);
        ll_vendor = (LinearLayout)findViewById(R.id.ll_vendor);
        ll_category = (LinearLayout)findViewById(R.id.ll_category);
        iv_previousDateBtn = (ImageView) findViewById(R.id.iv_previousDateBtn);
        tv_selectedDate = (TextView) findViewById(R.id.tv_selectedDate);
        iv_nextDateBtn = (ImageView) findViewById(R.id.iv_nextDateBtn);
        ll_daysOfWeeks = (LinearLayout) findViewById(R.id.ll_daysOfWeeks);
        tv_totalexpenses = (TextView)findViewById(R.id.tv_totalamount);
        TextView tv_sunText = (TextView) findViewById(R.id.tv_sunText);
        TextView tv_monText = (TextView) findViewById(R.id.tv_monText);
        TextView tv_tueText = (TextView) findViewById(R.id.tv_tueText);
        TextView tv_wedText = (TextView) findViewById(R.id.tv_wedText);
        TextView tv_thuText = (TextView) findViewById(R.id.tv_thuText);
        TextView tv_friText = (TextView) findViewById(R.id.tv_friText);
        TextView tv_satText = (TextView) findViewById(R.id.tv_satText);
        gv_calendar = (GridView) findViewById(R.id.gv_calendar);
        tv_errNoDataMsg = (TextView) findViewById(R.id.tv_errNoDataMsg);

        tv_pageTitle.setTypeface(SEMIBOLD);
        tv_categoryText.setTypeface(REGULAR);
        tv_category.setTypeface(REGULAR);
        tv_vendorText.setTypeface(REGULAR);
        tv_vendor.setTypeface(REGULAR);
        tv_selectedDate.setTypeface(SEMIBOLD);
        tv_sunText.setTypeface(REGULAR);
        tv_monText.setTypeface(REGULAR);
        tv_tueText.setTypeface(REGULAR);
        tv_wedText.setTypeface(REGULAR);
        tv_thuText.setTypeface(REGULAR);
        tv_friText.setTypeface(REGULAR);
        tv_satText.setTypeface(REGULAR);
        tv_errNoDataMsg.setTypeface(SEMIBOLD);

        ll_vendor.setOnClickListener(this);
        ll_category.setOnClickListener(this);
        iv_backBtn.setOnClickListener(this);
        iv_previousDateBtn.setOnClickListener(this);
        iv_nextDateBtn.setOnClickListener(this);
        iv_export.setOnClickListener(this);

        vendorsDataList=new ArrayList<CategoryListItem.Vendors>();
        for(int i=0;i<((AircountrApplication) getApplicationContext()).mCategoryListItems.size();i++){
            for(int j=0;j<((AircountrApplication) getApplicationContext()).mCategoryListItems.get(i).getVendorsDataList().size();j++){
                vendorsDataList.add(((AircountrApplication) getApplicationContext()).mCategoryListItems.get(i).getVendorsDataList().get(j));
            }
        }

    }

    private void selectOptionPopUp(final int btnType) {
        final Dialog dialog = new Dialog(CalendarActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_category_dropdown);
        ListView lv_category = (ListView) dialog.findViewById(R.id.lv_category);
        if (btnType == BTN_CATEGORY) {
            final ArrayList<CategoryListItem> categoryListItemsSpinner = new ArrayList<CategoryListItem>();
            CategoryListItem item = new CategoryListItem();
            item.setCategoryName("All Category");
            categoryListItemsSpinner.add(0, item);
            for(int i=1;i<=((AircountrApplication) getApplicationContext()).mCategoryListItems.size();i++){
                categoryListItemsSpinner.add(i,((AircountrApplication) getApplicationContext()).mCategoryListItems.get(i-1));
            }
            final CategorySpinnerAdapter mCategorySpinnerAdapter = new CategorySpinnerAdapter(CalendarActivity.this, categoryListItemsSpinner);
            lv_category.setAdapter(mCategorySpinnerAdapter);

            lv_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    tv_category.setText(categoryListItemsSpinner.get(position).getCategoryName());
                    tv_vendor.setText(getResources().getString(R.string.txt_all_vendors));
                    if(position!=0) {
                        categoryId = (categoryListItemsSpinner.get(position).getCategoryId());
                        vendorId = "";
                        vendorsDataList = new ArrayList<CategoryListItem.Vendors>(categoryListItemsSpinner.get(position).getVendorsDataList());
                        Log.d(TAG, "Vendor list size : " + vendorsDataList.size() + "position : " + position + "Main List Size : " + ((AircountrApplication) getApplicationContext()).mCategoryListItems.get(position-1).getVendorsDataList().size());
                        dialog.dismiss();
                        sendGetCalendarRequest(strTimeStamp);
                    }
                    else {
                        vendorsDataList=new ArrayList<CategoryListItem.Vendors>();
                        for(int i=0;i<((AircountrApplication) getApplicationContext()).mCategoryListItems.size();i++){
                            for(int j=0;j<((AircountrApplication) getApplicationContext()).mCategoryListItems.get(i).getVendorsDataList().size();j++){
                                vendorsDataList.add(((AircountrApplication) getApplicationContext()).mCategoryListItems.get(i).getVendorsDataList().get(j));
                            }
                        }
                        vendorId = "";
                        categoryId = "";
                        dialog.dismiss();
                        sendGetCalendarRequest(strTimeStamp);
                    }
                }
            });

            dialog.show();
        } else if (btnType == BTN_VENDOR) {
            VendorSpinnerAdapter mVendorSpinnerAdapter = new VendorSpinnerAdapter(CalendarActivity.this, vendorsDataList);
            lv_category.setAdapter(mVendorSpinnerAdapter);
            lv_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    tv_vendor.setText(vendorsDataList.get(position).getVendorName());
                    vendorId = vendorsDataList.get(position).getVendorId();
                    Log.d("Vendor Id", vendorId);
                    dialog.dismiss();
                    sendGetCalendarRequest(strTimeStamp);
                }
            });

            dialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_backBtn:
                CalendarActivity.this.finish();
                break;
            case R.id.ll_category:
                if (((AircountrApplication) getApplicationContext()).mCategoryListItems != null && ((AircountrApplication) getApplicationContext()).mCategoryListItems.size() > 0)
                    selectOptionPopUp(BTN_CATEGORY);
                else
                    displayToast("NO CATEGORY FOUND");
                break;
            case R.id.ll_vendor:
                if (vendorsDataList != null && vendorsDataList.size() > 0)
                    selectOptionPopUp(BTN_VENDOR);
                else
                    displayToast("NO VENDOR FOUND");
                break;
            case R.id.iv_previousDateBtn:
                cal.add(Calendar.MONTH, -1);
                tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));
                strTimeStamp = String.valueOf(cal.getTimeInMillis());
                sendGetCalendarRequest(String.valueOf(cal.getTimeInMillis()));
                break;
            case R.id.iv_nextDateBtn:
                cal.add(Calendar.MONTH, 1);
                tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));
                strTimeStamp = String.valueOf(cal.getTimeInMillis());
                sendGetCalendarRequest(String.valueOf(cal.getTimeInMillis()));
                break;
            case R.id.iv_export:
                if (strTimeStamp != null)
                    sendReportExportRequest();
                break;
        }
    }

    private void sendGetCalendarRequest(String timeStamp) {
        if (!AppPreferences.getMerchantId(CalendarActivity.this).equals("") && !AppPreferences.getAccessToken(CalendarActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new GetCalendarAsync().execute(timeStamp);
            } else
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        } else {
            showDialog("Alert", "You seems to be logged out", "OK");
        }
    }

    private class GetCalendarAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("Rahul Kumar", "All Good here");
            showLoading("Plz Wait...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG, "Response : " + s);
            try {
                if (s != null) {
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    if (success) {
                        total = 0;
                        tv_totalexpenses.setText(total+"");
                        JSONArray invoiceArray = response.getJSONArray("inv");
                        if (invoiceArray != null && invoiceArray.length() > 0) {
                            mCalendarDataList.clear();
                            for (int i = 0; i < invoiceArray.length(); i++) {
                                JSONObject invoiceObj = invoiceArray.getJSONObject(i);
                                CalendarListItem rowData = new CalendarListItem();
                                JSONObject dayObj = invoiceObj.getJSONObject("_id");
                                int day = dayObj.getInt("day");
                                String amount = invoiceObj.getString("totalinvoice");
                                int numberOfInvoice = invoiceObj.getInt("numberofinvoices");
                                JSONArray commentsArray = invoiceObj.getJSONArray("comments");
                                ArrayList<String> commentsList = new ArrayList<>();
                                if (commentsArray != null && commentsArray.length() > 0) {
                                    for (int j = 0; j < commentsArray.length(); j++) {
                                        String comment = commentsArray.getString(j);
                                        commentsList.add(comment);
                                    }
                                }
                                rowData.setDate(day);
                                rowData.setAmount(amount);
                                rowData.setNumberOfInvoice(numberOfInvoice);
                                rowData.setComments(commentsList);
                                mCalendarDataList.add(rowData);
                                total = total + Integer.parseInt(amount);

                            }
                            tv_totalexpenses.setText(total+"");
                            tv_errNoDataMsg.setVisibility(View.GONE);
                            ll_daysOfWeeks.setVisibility(View.VISIBLE);
                            gv_calendar.setVisibility(View.VISIBLE);
                        }
                    } else {
                        tv_totalexpenses.setText("0");
                        tv_errNoDataMsg.setVisibility(View.VISIBLE);
                        tv_errNoDataMsg.setText(response.getString("msg"));
                        ll_daysOfWeeks.setVisibility(View.GONE);
                        gv_calendar.setVisibility(View.GONE);
                    }
                } else {
                    showDialog("Alert", "oops! something is wrong", "OK");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mCalendarGridAdapter.onDataSetChanged(mCalendarDataList);
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = CreateUrl.getCalendarDataUrl(AppPreferences.getMerchantId(CalendarActivity.this), params[0], vendorId, categoryId);
            String _response = null;
            Log.d("Rahul Kumar", url);
            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 300000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", AppPreferences.getAccessToken(CalendarActivity.this));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d("Rahul Kumar", "All Good here");
            return _response;
        }
    }

    private void sendReportExportRequest() {
        if (!AppPreferences.getMerchantId(CalendarActivity.this).equals("") && !AppPreferences.getAccessToken(CalendarActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new ReportExportAsync().execute(strTimeStamp);
            } else
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        } else {
            showDialog("Alert", "You seems to be logged out", "OK");
        }
    }

    private class ReportExportAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Exporting...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject response = new JSONObject(s);
                boolean success = response.getBoolean("success");
                String msg = response.getString("msg");
                if (success)
                    displayToast(msg);
                else
                    showDialog("Alert", msg, "OK");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.exportReportsUrl();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(),
                    30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", AppPreferences.getAccessToken(CalendarActivity.this));
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("merchantId", AppPreferences.getMerchantId(CalendarActivity.this));
                jsonObject.put("timestamp", params[0]);
                httpPost.setEntity(new ByteArrayEntity(jsonObject.toString().getBytes("UTF8")));

                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }
    private void sendInvoiceListRequest(String timeStamp, String day) {
        if (!AppPreferences.getMerchantId(CalendarActivity.this).equals("") && !AppPreferences.getAccessToken(CalendarActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new InvoiceListAsync().execute(timeStamp, day );
            }
        }
    }
    private class InvoiceListAsync extends AsyncTask<String, Void , String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.d(TAG, "Response : " + s);
            if (s != null) {
                try {
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    if (success) {
                        JSONArray invoiceArray = response.getJSONArray("invoice");
                        if (invoiceArray != null && invoiceArray.length() > 0) {
                            int pos = getPosition();
                            Log.d(TAG, "T : " + strTimeStamp + " D : " + String.valueOf(mCalendarDataList.get(pos).getDate()));
                            Intent intent = new Intent(CalendarActivity.this, InvoiceListActivity.class);
                            intent.putExtra(TIMESTAMP, strTimeStamp);
                            intent.putExtra(DAY_OF_MONTH, String.valueOf(mCalendarDataList.get(pos).getDate()));
                            intent.putExtra(SELECTED_DATE, tv_selectedDate.getText());
                            startActivity(intent);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.invoiceListUrl(AppPreferences.getMerchantId(CalendarActivity.this), params[0], params[1]);
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", AppPreferences.getAccessToken(CalendarActivity.this));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }
    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
